# How it works


There are several methods shown in class `StartingJava`.

Study the Javadoc and follow the instructions; usually this means you 
 must implement the method body.

Verify the correctness of your solution by running the corresponding test method 
(to be found in `/src/test/java/section1_intro/part0_how_it_works/StartingJavaTest.java`)

